---
- name: Install - Create owncast_bot group
  group:
    name: "{{ owncast_bot_group }}"
    state: present
  become: true

- name: Install - Create owncast_bot user
  user:
    name: "{{ owncast_bot_user }}"
    group: "{{ owncast_bot_group }}"
  register: user_info
  become: true

- name: Install - previous task results
  debug:
    var: user_info
    verbosity: 2

- name: Install - Ensure container paths exist
  file:
    path: "{{ item.path }}"
    state: directory
    mode: 0777
    owner: "{{ owncast_bot_user }}"
    group: "{{ owncast_bot_group }}"
  with_items:
    - { path: "{{ owncast_bot_container_base_path }}", when: true }
  when: "item.when|bool"
  become: true

- name: Install - Ensure container image is pulled
  docker_image:
    name: "{{ owncast_bot_container_docker_image }}"
    source: "{{ 'pull' if ansible_version.major > 2 or ansible_version.minor > 7 else omit }}"
    force_source: "{{ owncast_bot_container_docker_image_force_pull if ansible_version.major > 2 or ansible_version.minor >= 8 else omit }}"
    force: "{{ omit if ansible_version.major > 2 or ansible_version.minor >= 8 else owncast_bot_container_docker_image_force_pull }}"
  register: result
  retries: "{{ owncast_bot_container_retries_count }}"
  delay: "{{ owncast_bot_container_retries_delay | int }}"
  until: result is not failed
  become: true

- name: Install - Ensure owncast_bot environment variables file created
  template:
    src: "{{ role_path }}/templates/env.j2"
    dest: "{{ owncast_bot_container_base_path }}/env"
    owner: "{{ owncast_bot_user }}"
    group: "{{ owncast_bot_group }}"
    mode: 0777
  become: true

- name: Install - Clone Skill Repository
  git:
    repo: "{{ owncast_bot_skill_repo }}"
    dest: "{{ owncast_bot_container_data_dir_path }}"
    version: "{{ owncast_bot_skill_repo_branch }}"
    ssh_opts: "-o StrictHostKeyChecking=no"
    force: true
  become: true

- name: Install - Ensure owncast_bot configuration file created
  template:
    src: "{{ role_path }}/templates/configuration.j2"
    dest: "{{ owncast_bot_container_data_dir_path }}/configuration.yaml"
    owner: "{{ owncast_bot_user }}"
    group: "{{ owncast_bot_group }}"
    mode: 0777
  become: true

- name: Install - Ensure {{ owncast_bot_name }}.service installed
  template:
    src: "{{ role_path }}/templates/systemd/bot.service.j2"
    dest: "{{ owncast_bot_systemd_path }}/{{ owncast_bot_name }}.service"
    mode: 0644
  register: owncast_bot_container_systemd_service_result
  become: true

- name: Install - Ensure systemd reloaded after {{ owncast_bot_name }}.service installation
  service:
    daemon_reload: true
  when: "owncast_bot_container_systemd_service_result.changed|bool"
  become: true
